/**
 *
 */
package fizzBuzz;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author egashira.kentaro
 *
 */
public class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */

	@Test
	//引数に９を渡した場合「Fizz」
	public void checkFizzBuzz_01() {
		System.out.println("checkFizzBuzz_01");
		assertEquals(FizzBuzz.checkFizzBuzz(9), "Fizz");
	}

	@Test
	//引数に20を渡した場合「Buzz」
	public void checkFizzBuzz_02() {
		System.out.println("checkFizzBuzz_02");
		assertEquals(FizzBuzz.checkFizzBuzz(20), "Buzz");
	}

	@Test
	//引数に45を渡した場合「FizzBuzz」
	public void checkFizzBuzz_03() {
		System.out.println("checkFizzBuzz_03");
		assertEquals(FizzBuzz.checkFizzBuzz(45), "FizzBuzz");
	}

	@Test
	//引数に44を渡した場合「44」
	public void checkFizzBuzz_04() {
		System.out.println("checkFizzBuzz_04");
		assertEquals(FizzBuzz.checkFizzBuzz(44), "44");
	}

	@Test
	//引数に46を渡した場合「46」
	public void checkFizzBuzz_05() {
		System.out.println("checkFizzBuzz_05");
		assertEquals(FizzBuzz.checkFizzBuzz(46), "46");
	}




}
